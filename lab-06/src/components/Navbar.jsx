import React from "react";
import {Box} from "@mui/material";
import {Link, Outlet, useParams} from "react-router-dom";
import styled from "styled-components";

const MyLink = styled.button`
        font-size: 1.5rem;
        text-align: center;
        color: white;
        background: black;
        cursor: pointer;
        margin-right: 20px;
        margin-top: 20px;
    `;

const MyBox = styled.div`
        margin-bottom: 20px;
    `;

export const Navbar = () => {
    const {category, id} = useParams();

    console.log(category, id);

    return (
        <>
            <MyBox>
                <Box sx={{display: "flex", alignItems: "center", textAlign: "center"}}>
                    <Link to="/" sx={{minWidth: 100}}><MyLink>Home page</MyLink></Link>
                    {
                        category && <MyLink disabled>{category}</MyLink>
                    }
                    {
                        id && <MyLink disabled>{id}</MyLink>
                    }
                </Box>
            </MyBox>
            <Outlet/>
        </>
    );
};