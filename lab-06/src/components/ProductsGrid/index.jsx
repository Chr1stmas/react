import {Link, Outlet, useNavigate, useParams, useSearchParams} from "react-router-dom";
import React, {useEffect, useState} from "react";
import styles from './ProductsGrid.module.css';
import {TextField} from "@mui/material";

export const ProductsGrid = () => {
    const {category} = useParams();
    const [products, setProducts] = useState([]);
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams();
    const [stateCategory, setStateCategory] = useState();
    let q = searchParams.get('q');

    useEffect(() => {
        setStateCategory(category)
        if (q) {
            fetch(`https://dummyjson.com/products/search?q=${q}`)
                .then((response) => response.json())
                .then((data) => setProducts(data.products));
        } else {
            fetch(`https://dummyjson.com/products/category/${category}`)
                .then((response) => response.json())
                .then((data) => setProducts(data.products));
        }
    }, [q]);

    const handleChange = event => {
        setSearchParams({q: event.target.value});
    };

    const handleFocus = event => {
        if (event.target.value.length === 0) {
            navigate(`/category/${stateCategory}/products`)
            console.log(stateCategory)
        }
    };

    return (
        <>
            <div>
                <TextField
                    variant="outlined"
                    fullWidth
                    label="Search"
                    onChange={handleChange}
                    onBlur={handleFocus}
                />
                <div className={styles.row}>
                    {products.map(product =>
                        <Link key={product.id} to={`/product/${product.id}`}>
                            <div className={styles.productItem}>
                                <img src={product.thumbnail} alt=""/>
                                <div className={styles.productList}>
                                    <h3>{product.title}</h3>
                                    <span className={styles.price}>{product.price} грн</span>
                                </div>
                            </div>
                        </Link>)
                    }
                </div>
            </div>
        </>
    );
};