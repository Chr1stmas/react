import React, {useEffect, useState} from "react";
import {Button, Grid} from "@mui/material";
import {Link, Outlet} from "react-router-dom";


export function Categories(props) {
    const {categories} = props;
    const [allCategories, setAllCategories] = useState([]);

    useEffect(() => {
        fetch(categories)
            .then((response) => response.json())
            .then((responseJson) => setAllCategories(responseJson))
            .catch((error) => {
                console.error(error)
            })
    }, [categories]);

    return (
        <>
            <Grid container spacing={2}>
                {allCategories.map(elem =>
                    <Grid item xs={3} key={elem}>
                        <Link to={`/category/${elem}/products`}>
                            <Button variant="outlined">{elem}</Button>
                        </Link>
                    </Grid>)}
            </Grid>
        </>
    );
}