import './App.css';
import {Container} from "@mui/material";
import {Categories} from "./components/Categories";
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import {Navbar} from "./components/Navbar"
import {ProductsGrid} from "./components/ProductsGrid";
import {Product} from "./components/Product";

const categories = "https://dummyjson.com/products/categories";

function App() {
    const router = createBrowserRouter([
        {
            path: "/",
            element: <Navbar/>,
            children: [
                {
                    path: "/",
                    element: <Categories categories={categories}/>,
                },
                {
                    path: "/category/:category/products",
                    element: <ProductsGrid/>,
                },
                {
                    path: "/product/:id",
                    element: <Product/>,
                },
                {
                    path: "*",
                    element: <h1>404</h1>,
                },
            ],
        },
    ]);
  return (
      <Container fixed>
          <RouterProvider router={router}/>
      </Container>
  );
}

export default App;
