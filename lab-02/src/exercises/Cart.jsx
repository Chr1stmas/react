import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import Button from "@mui/material/Button";
import * as React from "react";
import {useState} from "react";


const MyTable = (pr) => {

    const {name, price, min, max} = pr.data;
    const [count, setCount] = useState(min);

    function Plus() {
        setCount(count + 1);
        pr.setTotals(
            {
                totalCount: pr.data.totalCount + 1,
                totalSum: pr.data.totalSum + price,
            }
        );
    }

    function Minus() {
        setCount(count - 1);
        pr.setTotals(
            {
                totalCount: pr.data.totalCount - 1,
                totalSum: pr.data.totalSum - price,
            }
        );
    }

    return (
        <TableRow>
            <TableCell>{name}</TableCell>
            <TableCell>{price}</TableCell>
            <TableCell>
                <Button variant="outlined" onClick={Minus} disabled={count === min}>-</Button>
                {count}
                <Button variant="outlined" onClick={Plus} disabled={count === max}>+</Button>
            </TableCell>
            <TableCell>
                {count * price}
            </TableCell>
        </TableRow>
    )
}

const Cart = (data) => {
    const {products} = data;

    function TotalCount(prod) {
        let totalCount = 0
        prod.forEach((product) => {
            totalCount += product.min;
        });
        return totalCount;
    }

    function TotalSum(prod) {
        let totalSum = 0
        prod.forEach((product) => {
            totalSum += product.price * product.min
        });
        return totalSum;
    }

    const [total, setTotals] = useState({
        totalCount: TotalCount(products),
        totalSum: TotalSum(products)
    });

    return (
        <TableContainer component={Paper}>
            <Table sx={{minWidth: 600}} aria-label="customized table">
                <TableHead style={{background: "lightgreen"}}>
                    <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell>Price</TableCell>
                        <TableCell>Quantity</TableCell>
                        <TableCell>Total</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        products.map((product) => (<MyTable key={product.id} data={product} setTotals={setTotals}/>))
                    }
                    <TableRow style={{background: "lightblue"}}>
                        <TableCell colSpan={2}>Totals</TableCell>
                        <TableCell>
                            {total.totalCount}
                        </TableCell>
                        <TableCell>
                            {total.totalSum}
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default Cart;