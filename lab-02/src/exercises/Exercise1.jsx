import * as React from 'react';
import Button from '@mui/material/Button';
import {Stack} from "@mui/material";
import {useState} from "react";

const MyCounter = (counter) => {
    const {initial, min, max} = counter.data
    const [total, setTotal] = useState(initial || 0);

    function Plus() {
        setTotal(total + 1);
    }

    function Minus() {
        setTotal(total - 1);
    }

    function Reset() {
        setTotal(initial || 0)
    }

    return (
        <Stack direction="row" spacing={2} key={counter.id}>
            <h2 key={counter.id}>
                Поточний рахунок: {total}
            </h2>
            <Button variant="outlined" onClick={Plus} disabled={total === max}>+</Button>
            <Button variant="outlined" onClick={Minus} disabled={total === min}>-</Button>
            <Button variant="outlined" onClick={Reset}>Reset</Button>
        </Stack>
    );
}

export default MyCounter;