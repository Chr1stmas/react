import './App.css';
import Exercise1 from "./exercises/Exercise1";
import * as React from "react";
import Cart from "./exercises/Cart";
import GuessTheNumber from "./exercises/GuessTheNumber";

const counters = [
    {id: 1, initial: 6, min: -5, max: 10},
    {id: 2, initial: 5},
    {id: 3},
];

const products = [
    {id: 1, name: "Constructor Lego", price: 300, min: 1, max: 5},
    {id: 2, name: "Train Station", price: 200, min: 1, max: 2},
    {id: 3, name: "Hot Wheels Track", price: 150, min: 2, max: 10},
];


function App() {
    return (
        <div className="App">
            <div>
                {
                    counters.map(counter => <Exercise1 data={counter}/>)
                }
            </div>
            <Cart products={products}/>
            <GuessTheNumber/>
        </div>
    );
}

export default App;
