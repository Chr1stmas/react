import './App.css';
import ReactVirtualizedTable from "./components/ReactVirtualizedTable";
import {useEffect, useState} from "react";

function App() {
    const [photos, setPhotos] = useState([]);

    useEffect(() => {
        fetch("https://jsonplaceholder.typicode.com/photos")
            .then((res) => res.json())
            .then(
                (data) => {
                    let res = data.filter(word => word.title.split(' ').length < 8);
                    setPhotos(res);
                }
            )
    }, []);

    return (
        <ReactVirtualizedTable photos={photos}/>
    );
}

export default App;
