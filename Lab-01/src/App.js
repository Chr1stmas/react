import logo from './logo.svg';
import './App.css';
import React from "react";
import ReactDOM from "react-dom";
import ArrayOfCities from "./exercises/ArrayOfCities";
import AComponentAsAFunction from "./exercises/AComponentAsAFunction";
import BreakdownIntoSubcomponents from "./exercises/BreakdownIntoSubcomponents";
import Table from "./exercises/Table";
import ChangingTheStateOfAComponent from "./exercises/ChangingTheStateOfAComponent";

function App() {
  return (
      <div>
          <AComponentAsAFunction />
          <ArrayOfCities />
          <BreakdownIntoSubcomponents />
          <Table />
          <ChangingTheStateOfAComponent />
      </div>
  );
}

export default App;
