import React, {useState} from "react";
import {Select} from "@mui/material";

const cities = [
    {
        id: 1,
        name: "Chicago",
        image: 'https://www.viajarchicago.com/img/informacion-sobre-chicago.jpg'
    },
    {
        id: 2,
        name: "Los Angeles",
        image: 'https://image.arrivalguides.com/415x300/15/77dea2cec7c1b3f8d9418c932208bfb2.jpg'
    },
    {
        id: 3,
        name: "New York",
        image: 'https://worldstrides.com/wp-content/uploads/2015/07/iStock_000040849990_Large.jpg'
    },
]

const City = (props) => {
    return <option>{props.city.name}</option>
}

const ShowImg = (props) => {
    return <img style={{width: '100px', height: '100px'}} src={props.image}></img>
}

const List = (props) => {
    const showMore = null
    return (
        <>
            <select name="" id="cities">
                {props.data.map((city) => <>
                        showMore = city
                        <City key={city.id} city={city}></City>
                    </>
                )}
            </select>
            <img style={{width: '100px', height: '100px'}}></img>
        </>
    )
}

const ArrayOfCities = () => {
    return (
        <List data={cities}></List>
    );
}

export default ArrayOfCities;