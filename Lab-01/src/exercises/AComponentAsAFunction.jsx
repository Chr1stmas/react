import React from "react";

const product1 = {name: "Jery", tt: "mouse"}

const Product = (props) => {
    return (
        <div>
            I'm a {props.product.name} and I am a {props.product.tt}
        </div>
    );
}

const AComponentAsAFunction = () => {
    return (
        <div>
            <Product product={product1} />
        </div>
    );
}

export default AComponentAsAFunction;