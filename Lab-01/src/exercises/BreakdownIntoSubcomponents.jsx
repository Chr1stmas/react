import React, {useState} from "react";
import {Button, ListGroup, Card} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const products = [
    {
        id: 1,
        title: 'Ноутбук Lenovo V14 G2 ITL (Intel i3-1115G4/8/128F/int/W10Pro) Black',
        newprice: '16 999',
        oldprice: '19 999',
        img: 'https://content.rozetka.com.ua/goods/images/big/274406559.jpg',
        InStock: 'Є в наявності',
    },
    {
        id: 2,
        title: 'Ноутбук ASUS TUF Gaming F15 FX506HC-HN006 (90NR0723-M01150) Eclipse Gray / Intel Core i5-11400H /',
        newprice: '41 999',
        oldprice: '49 999',
        img: 'https://content2.rozetka.com.ua/goods/images/big/278076445.jpg',
        InStock: 'Є в наявності',
    },
    {
        id: 3,
        title: 'Ноутбук Acer Aspire 7 A715-75G-569U (NH.Q87EU.004) Charcoal Black',
        newprice: '30 999',
        oldprice: null,
        img: 'https://content2.rozetka.com.ua/goods/images/big/136846086.jpg',
        InStock: null,
    },
    {
        id: 4,
        title: 'Ноутбук Apple MacBook Air 13" M1 256GB 2020 (MGN63) Space Gray',
        newprice: '41 999',
        oldprice: '49 999',
        img: 'https://content1.rozetka.com.ua/goods/images/big/144249716.jpg',
        InStock: 'Є в наявності',
    },
    {
        id: 5,
        title: 'Ноутбук ASUS Laptop X415JA-EB2223 (90NB0ST1-M010H0) Transparent Silver / Intel Core i3-1005G1 / RAM 8 ГБ / SSD 256 ГБ',
        newprice: '18 999',
        oldprice: '20 999',
        img: 'https://content1.rozetka.com.ua/goods/images/big/276242437.jpg',
        InStock: 'Є в наявності',
    },
    {
        id: 6,
        title: 'Ноутбук ASUS TUF Gaming F15 FX506HC-HN006 (90NR0723-M01150) Eclipse Gray / Intel Core i5-11400H /',
        newprice: '41 999',
        oldprice: '49 999',
        img: '2.jpg',
        InStock: 'Є в наявності',
    },
    {
        id: 7,
        title: 'Ноутбук ASUS TUF Gaming F15 FX506HC-HN006 (90NR0723-M01150) Eclipse Gray / Intel Core i5-11400H /',
        newprice: '41 999',
        oldprice: '49 999',
        img: '2.jpg',
        InStock: 'Є в наявності',
    },
    {
        id: 8,
        title: 'Ноутбук ASUS TUF Gaming F15 FX506HC-HN006 (90NR0723-M01150) Eclipse Gray / Intel Core i5-11400H /',
        newprice: '41 999',
        oldprice: '49 999',
        img: '2.jpg',
        InStock: null,
    },
    {
        id: 9,
        title: 'Ноутбук ASUS TUF Gaming F15 FX506HC-HN006 (90NR0723-M01150) Eclipse Gray / Intel Core i5-11400H /',
        newprice: '41 999',
        oldprice: '49 999',
        img: '2.jpg',
        InStock: null,
    },
    {
        id: 10,
        title: 'Ноутбук ASUS TUF Gaming F15 FX506HC-HN006 (90NR0723-M01150) Eclipse Gray / Intel Core i5-11400H /',
        newprice: '41 999',
        oldprice: '49 999',
        img: '2.jpg',
        InStock: 'Є в наявності',
    },

]

const CardA = (props) => {
    return (
        <Card style={{width: '18rem'}}>
            <Card.Img variant="top" src={props.product.img}/>
            <Card.Body>
                <Card.Title>{props.product.title}</Card.Title>
                {props.product.oldprice
                    ? <div>
                        <Card.Text>
                            <small className="text-muted">
                                <del>{props.product.oldprice} ₴</del>
                            </small>
                        </Card.Text>
                        <Card.Text style={{color: 'red'}}>{props.product.newprice} ₴</Card.Text>
                    </div>
                    : <Card.Text>{props.product.newprice} ₴</Card.Text>
                }
                {
                    props.product.InStock
                        ? <small className="text-muted">{props.product.InStock}</small>
                        : <small className="text-muted"></small>
                }
            </Card.Body>
        </Card>
    )
}

const AllCard = (props) => {

    const [showMore, setShowMore] = useState(false)

    const numberOfItems = showMore ? products.length : 5;
    return (
        <div className="card-group">
            {props.data.slice(0, numberOfItems).map((card) => <CardA key={card.id} product={card}></CardA>)}
            {showMore
                ? <button onClick={() => setShowMore()}>Показати менше</button>
                : <button onClick={() => setShowMore(true)}>Показати більше</button>
            }
        </div>
    )
}

const BreakdownIntoSubcomponents = () => {
        return (
            <AllCard data={products}></AllCard>
        )
}

export default BreakdownIntoSubcomponents;