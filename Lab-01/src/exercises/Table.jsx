import ProductApp from "./ChangingTheStateOfAComponent";

const rows = [{
    dataField: 'FirstName',
    text: 'John'
}, {
    dataField: 'LastName',
    text: 'Silver'
}, {
    dataField: 'Occupation',
    text: 'Private Captain'
}];

const Table = () => {
    return(
            <table>
                {
                    rows.map((row) => (
                <tr><th>{row.dataField}</th><td>{row.text}</td></tr>
                    ))
                }
            </table>
    )
}

export default Table;