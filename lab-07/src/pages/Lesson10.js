import { useDispatch, useSelector } from "react-redux";
import {decreaseAction, increaseAction} from "../store/reducers/counterReducer";
import {addUserAction, removeUserAction} from "../store/reducers/userReducer";
import {fetchUsers} from "../store/actions/users";

const payload = 10;

export const Lesson10 = () => {
    const dispatch = useDispatch();
    const counter = useSelector((state) => state.counterReducer.counter);
    const topic = useSelector((state) => state.counterReducer.lesson.topic);
    const users = useSelector((state) => state.userReducer.users);

    const increase = () => {
        dispatch(increaseAction(payload));
    }
    const decrease = () => {
        dispatch(decreaseAction(payload));
    }

    const addUser = () => {
        const user = {
            name: `Petya ${Date.now()}`,
            id: Date.now()
        }
        dispatch(addUserAction(user));
    }

    const removeUser = (id) => {
        dispatch(removeUserAction(id));
    }

    return (
        <div>
            <div>
                Lesson: {topic}

                <br/>
                Counter: {counter}
                <button onClick={increase}>Increase counter</button>
                <button onClick={decrease}>Decrease counter</button>
            </div>

            <div>
                <button onClick={addUser}>Add user</button>
                <button onClick={() => { dispatch(fetchUsers()) }}>Add users from fakeAPI</button>

                {users.length ?
                    <div>
                        <h3>Users</h3>
                        {users.map(user =>
                            <div id={user.id} key={user.id} onClick={() => removeUser(user.id)}>{user.name}</div>)}
                    </div>
                    :
                    <div>Нікого немає вдома</div>
                }
            </div>
        </div>
    )
};