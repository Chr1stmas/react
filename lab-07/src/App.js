import './App.css';
import {Lesson10} from './pages/Lesson10';
import {createBrowserRouter, createRoutesFromElements, Route, RouterProvider} from 'react-router-dom';

function App() {
    const router = createBrowserRouter(
        createRoutesFromElements(
            <Route path="/" element={<Lesson10/>}>Home</Route>
        )
    );

    return (
        <RouterProvider fallback="<div>sdfsdf</div>" router={router}/>
    );
}

export default App;
