import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    users: [],
};
const changeId = (arr) => {
    var randomstring = require("randomstring");
    return arr.map(el => ({...el, id: randomstring.generate({
            length: 8,
            charset: 'numeric'
        })
    }))
}

export const userReducer =  createSlice({
    name: "user",
    initialState,
    reducers: {
        addUserAction: (state, action) => {
            state.users = [...state.users, action.payload];
            console.log(state.users);
        },
        removeUserAction: (state , action) => {
            state.users = state.users.filter(user => user.id !== action.payload);
            console.log(state.users);
        },
        addUsersAction: (state , action) => {
            state.users = [...state.users, ...changeId(action.payload)];
            console.log(state.users);
        },
    }
});

export const {addUserAction, removeUserAction, addUsersAction} = userReducer.actions;
export default userReducer.reducer;