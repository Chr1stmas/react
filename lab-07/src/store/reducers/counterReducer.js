import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    counter: 20,
    lesson: {
        topic: "Redux Toolkit"
    },
};

export const counterReducer = createSlice({
    name: "counter",
    initialState,
    reducers: {
        increaseAction: (state, action) => {
            state.counter += Number(action.payload);
        },
        decreaseAction: (state, action) => {
            state.counter -= Number(action.payload);
        },
    },
});

export const {increaseAction, decreaseAction} = counterReducer.actions;

export default counterReducer.reducer;