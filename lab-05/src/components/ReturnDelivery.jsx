import {Controller} from "react-hook-form";
import {FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import React from "react";

export const ReturnDelivery = ({control, errors, returnType, returnTypeOptions}) => {

    return (
        <>
            <Controller
                name="returnType"
                id="returnType"
                defaultValue=""
                control={control}
                render={({field}) => (
                    <FormControl>
                        <InputLabel id={"returnTypeLabel"}>Тип палети</InputLabel>
                        <Select {...field} labelId={"returnTypeLabel"}
                                error={errors?.returnType?.type === "required"}>
                            {returnTypeOptions.map(option => <MenuItem key={option} value={option}>{option}</MenuItem>)}
                        </Select>
                    </FormControl>
                )}
            />{returnType === "Грошовий переказ" ?
            <Controller
                name="sum"
                id="sum"
                control={control}
                render={({field}) =>
                    <TextField {...field}
                               label="Сума"
                               type="number"
                               InputProps={{inputProps: {min: 0}}}
                    />}
            />
            :<></>
        }
            {errors?.returnType?.type === "required" && <p style={{color: "red"}}>Необхідно вказати тип</p>}
        </>
    );
};