import React, {useState} from 'react';


export default function Form() {
    const [value, setValue] = useState({});

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log(value);
    }

    const handleChange = (event) =>{
        const name = event.target.name;
        const value = event.target.value;
        setValue(values => ({...values, [name]: value}));
    }

    return (
        <>
            <h1>Завдання №1</h1>
            <form onSubmit={handleSubmit}>
                <input
                    type="text"
                    name="name"
                    placeholder="Ім'я"
                    value={value.name || ""}
                    onChange={handleChange}
                />
                <br/>
                <input
                    type="email"
                    name="email"
                    placeholder="E-mail*"
                    required
                    value={value.email || ""}
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                    onChange={handleChange}
                />
                <br/>
                <input
                    type="text"
                    name="topic"
                    placeholder='Тема*'
                    required
                    value={value.topic || ""}
                    onChange={handleChange}
                />
                <br/>
                <textarea
                    name='message'
                    placeholder='Повідомлення'
                    rows={13}
                    cols={30}
                    value={value.message}
                    onChange={handleChange}
                />
                <br/>
                <input type="submit"/>
            </form>
        </>
    );
}