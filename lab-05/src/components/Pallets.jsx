import React from "react";
import {Button, FormControl, Grid, IconButton, InputLabel, MenuItem, Select, TextField, Tooltip} from "@mui/material";
import {Controller} from "react-hook-form";
import HighlightOffOutlinedIcon from "@mui/icons-material/HighlightOffOutlined";

export const Pallets = ({
                            control,
                            options,
                            errors,
                            fields,
                            append,
                            remove
                        }) => {
    const addPlace = () => {
        append({
            palletType: "",
            announcedPrice: "",
            placeCount: ""
        })
    }

    return (
        <>
            {fields.map((item, i) =>
                <Grid container key={i}>
                    <Grid xs={3}>
                        <Controller
                            name={`characteristic[${i}].palletType`}
                            defaultValue=""
                            label="Тип палету"
                            control={control}
                            render={({field}) => (
                                <FormControl fullWidth>
                                    <InputLabel>Тип палету</InputLabel>
                                    <Select
                                        {...field} required labelId={`palletTypeLabel${i}`}>
                                        {options.map(option => <MenuItem key={option}
                                                                         value={option}>{option}</MenuItem>)}
                                    </Select>
                                </FormControl>
                            )}
                        />
                    </Grid>
                    <Grid xs={1}>
                        <Controller
                            name={`characteristic[${i}].announcedPrice`}
                            control={control}
                            render={({field}) =>
                                <TextField fullWidth error={errors?.characteristic?.[i]?.announcedPrice}
                                           {...field}
                                           label="Оголошена вартість"
                                />}
                        />
                    </Grid>
                    <Grid xs={1}>
                        <Controller
                            name={`characteristic[${i}].placeCount`}
                            control={control}
                            render={({field}) =>
                                <TextField required
                                           error={errors?.characteristic?.[i]?.placeCount}
                                           {...field}
                                           label="Кількість"
                                           type="number"
                                           InputProps={{inputProps: {min: 1}}}
                                />}
                        />
                    </Grid>
                    {
                        i !== 0 && <Tooltip title="Delete">
                            <IconButton>
                                <HighlightOffOutlinedIcon onClick={() => remove(i)}/>
                            </IconButton>
                        </Tooltip>
                    }
                </Grid>
            )}
            <Button onClick={addPlace}>Додати місце</Button>
        </>
    );
};
