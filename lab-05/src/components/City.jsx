import React from "react";
import {Controller} from "react-hook-form";
import {InputLabel, MenuItem, Select} from "@mui/material";

export const City = ({control, options, errors, name, typeName}) => {
    return (
        <>
            <InputLabel>{typeName}</InputLabel>
            <Controller
                name={name}
                id={name}
                defaultValue=""
                label={typeName}
                control={control}
                render={({field}) => (
                    <Select
                        {...field} error={errors?.[name]?.type === "required"}>
                        {options.map(option => <MenuItem key={option} value={option}>{option}</MenuItem>)}
                    </Select>
                )}
            />
            {errors?.[name]?.type === "required" && <p style={{color: "red"}}>Необхідно вказати місто</p>}
        </>
    );
};