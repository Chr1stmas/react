import {Checkbox, FormControlLabel, Grid, InputLabel, TextField} from "@mui/material";
import React from "react";
import {Controller} from "react-hook-form";


export const Upstairs = ({control}) => {
    return (
        <>
            <Grid container>
                <Grid xs={2}>
                    <p>Послуга "Підйом на поверх"</p>
                </Grid>
                <Grid xs={2}>
                    <Controller
                        name="floor"
                        control={control}
                        render={({field}) =>
                            <TextField {...field}
                                       label="Кількість поверхів"
                                       type="number"
                                       InputProps={{inputProps: {min: 0}}}
                            />}
                    />
                </Grid>
                <Grid xs={1}>
                    <Controller
                        name="isElevator"
                        defaultValue=""
                        control={control}
                        render={({field}) => <FormControlLabel {...field} control={<Checkbox/>}
                                                               label='Ліфт'/>}
                    />
                </Grid>
            </Grid>
        </>
    );
};