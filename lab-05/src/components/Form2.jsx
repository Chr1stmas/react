import {Controller, useFieldArray, useForm} from "react-hook-form";
import React, {useEffect} from "react";
import {Button, Checkbox, FormControl, FormControlLabel, InputLabel, MenuItem, Select} from "@mui/material";
import {City} from "../components/City";
import {Pallets} from "./Pallets";
import {Parcels} from "./Parcels";
import {yupResolver} from "@hookform/resolvers/yup";
import * as yup from "yup";
import {Packaging} from "./Packaging";
import {Upstairs} from "./Upstairs";
import {ReturnDelivery} from "./ReturnDelivery";

//Для виводу пустих початкових полів
const startData = {
    characteristic: [{
        palletType: "",
        placeCount: "",
        announcedPrice: "",
        weight: "",
        length: "",
        width: "",
        height: ""
    }]
};

const schema = yup.object().shape({
    recipient: yup.string().required(),
    departure: yup.string().required(),
    deliveryType: yup.string().required(),
    characteristic: yup.array().of(
        yup.object().shape({
            palletType: yup.string(),
            announcedPrice: yup.number().positive(),
            placeCount: yup.number().integer().positive(),
            weight: yup.number().positive(),
            length: yup.number().positive(),
            width: yup.number().positive(),
            height: yup.number().positive()
        })
    ),
    isElevator: yup.boolean(),
    returnType: yup.string().when("isReturn", {
        is: true,
        then: yup.string().required()
    }),
    floor: yup.number().positive().when("isElevator", {
        is: true,
        then: yup.number().required()
    }),
    sum: yup.number().integer().positive(),
});

export const Form2 = ({
                          cityOptions,
                          deliveryTypeOptions,
                          palletTypeOptions,
                          packingOptions,
                          returnTypeOptions
                      }) => {
    const {control, handleSubmit, watch, reset, formState: {errors}} = useForm({
        resolver: yupResolver(schema),
        shouldUnregister: true,
        defaultValues: {
            isPacking: false,
            isElevator: false,
            isReturn: false
        }

    });
    const onSubmit = data => console.log(data);

    const {fields, append, remove} = useFieldArray({
        control,
        name: "characteristic"
    });
    const deliveryType = watch("deliveryType");//для відображення
    const isPacking = watch("isPacking");//для відображення
    const isReturn = watch("isReturn");//для відображення
    const returnType = watch("returnType");//для відображення

    useEffect(() => {
        reset(startData);
    }, []);

    return (
        <>
            <h1>Завдання №2</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <FormControl variant="standard" sx={{m: 1, minWidth: 150}}>
                        <City control={control} options={cityOptions} errors={errors} name="departure"
                              typeName="Місто-відправки"/>
                    </FormControl>
                    <FormControl variant="standard" sx={{m: 1, minWidth: 150}}>
                        <City control={control} options={cityOptions} errors={errors} name="recipient"
                              typeName="Місто-одержувач"/>
                    </FormControl>
                </div>
                <div>
                    <div>
                        <InputLabel>Вид відправлення</InputLabel>
                        <Controller
                            name="deliveryType"
                            id="deliveryType"
                            defaultValue={deliveryTypeOptions[0]}
                            control={control}
                            render={({field}) => (
                                <Select
                                    {...field}>
                                    {deliveryTypeOptions.map(option => <MenuItem key={option}
                                                                                 value={option}>{option}</MenuItem>)}
                                </Select>)}
                        />
                    </div>
                    <div>
                        {deliveryType === "Палети" ?
                            <Pallets control={control}
                                     options={palletTypeOptions}
                                     errors={errors}
                                     fields={fields}
                                     append={append}
                                     remove={remove}/>
                            :
                            <Parcels control={control}
                                     errors={errors}
                                     fields={fields}
                                     append={append}
                                     remove={remove}/>
                        }
                    </div>
                    <div>
                        <Controller
                            name="isPacking"
                            control={control}
                            render={({field}) =>
                                <FormControlLabel {...field} control={<Checkbox/>} label='Послуга "Пакування"'/>}
                        />
                        {isPacking && <Packaging control={control} fields={fields} packingOptions={packingOptions}/>}
                        <Upstairs control={control}/>
                    </div>
                    <div>
                        <Controller
                            name="isReturn"
                            control={control}
                            render={({field}) =>
                                <FormControlLabel {...field} control={<Checkbox/>} label='Послуга "Зворотна доставка"'/>}
                        />
                        {isReturn && <ReturnDelivery control={control} returnType={returnType} errors={errors} returnTypeOptions={returnTypeOptions}/>}
                    </div>
                </div>
                <div className="form-example">
                    <Button variant="contained" color="success" type="submit">Реалізувати</Button>
                    <Button onClick={reset} variant="outlined" color="error">Очистити</Button>
                </div>
            </form>
        </>
    );
}
/*
name="CityOfDispatch" id="CityOfDispatch" value={value} onChange={handleChange} required
*/