import './App.css';
import Form1 from "./components/Form1";
import {Form2} from "./components/Form2";

function App() {
    const cities = ["Житомир", "Київ", "Одеса", "Львів", "Харків", "Херсон", "Вінниця"];
    const palletType = ["Палета від 1,5 м2 до 2 м2 (814)", "Палета від 1,5 м2 до 2 м2 (815)", "Палета від 1,5 м2 до 2 м2 (816)"];
    const deliveryType = ["Посилки", "Палети"];
    const returnType = ["Документи", "Грошовий переказ"];
    const packing = ["Конверт з ПБ плівкою С/13 (150х215) мм", "Конверт з ПБ плівкою D/14 (180х265) мм", "Конверт з ПБ плівкою E/15 (220х265) мм"];

    return (
    <div className="App">
        <Form1/>
        <Form2 cityOptions={cities} deliveryTypeOptions={deliveryType} palletTypeOptions={palletType}
               packingOptions={packing} returnTypeOptions={returnType}/>
    </div>
  );
}

export default App;
